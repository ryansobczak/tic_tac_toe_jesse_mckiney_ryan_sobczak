#ifndef CLIENT_H
#define CLIENT_H
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include "CloudClient.h"
#include "RakPeerInterface.h"
#include "RakNetTypes.h"
#include "RakSleep.h"
#include "GetTime.h"
#include "Board.h"
#include <stdlib.h>

#define SERVER_PORT 200

class Client
{
public:
	Client();
	~Client();

	bool GetServer();
	void Wait();
	void Send(BoardMain* pC);

	BoardMain GetInput();

private:
	RakNet::RakPeerInterface *pClient;
	BoardMain data;
	RakNet::SystemAddress ServerAddress;
};
#endif
