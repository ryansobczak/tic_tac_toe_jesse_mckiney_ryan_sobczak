#pragma once

#include<string>
#define CLIENT_PORT "202"

#include "RakNetTypes.h"

class Server
{
public:
	Server(std::string ipaddress);
	~Server();

	bool connectToServer(std::string ipAddress);
	void processData(RakNet::Packet* p);
	unsigned char GetPacketIdentifier(RakNet::Packet* p);
	void closeServer();

private:
	RakNet::RakPeerInterface *mpServer;
	std::string mServerIP;
};

