#include "Client.h"
#include "Board.h"
#include <iostream>
#include <string>

using namespace std;

Client::Client()
{
	pClient = RakNet::RakPeerInterface::GetInstance();
};
Client::~Client(){};

bool Client::GetServer()
{
	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.port = 0;

	string IPAddress;
	cout << "Enter Server IP: ";
	cin >> IPAddress;

	char remoteIP[128];
	IPAddress._Copy_s(remoteIP, 128,128,0);
	pClient->Startup(1, &socketDescriptor, 1);
	pClient->Connect(remoteIP, (unsigned short)SERVER_PORT, 0, 0, 0);
	return true;
};
void Client::Wait()
{
	RakNet::Packet *p = pClient->Receive();
	while (p)
	{
		switch (p->data[0])
		{
		case ID_CONNECTION_REQUEST_ACCEPTED:
			printf("ID_CONNECTION_REQUEST_ACCEPTED\n");
			break;
			// print out errors
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("Client Error: ID_CONNECTION_ATTEMPT_FAILED\n");
			break;
		case ID_ALREADY_CONNECTED:
			printf("Client Error: ID_ALREADY_CONNECTED\n");
			break;
		case ID_CONNECTION_BANNED:
			printf("Client Error: ID_CONNECTION_BANNED\n");
			break;
		case ID_INVALID_PASSWORD:
			printf("Client Error: ID_INVALID_PASSWORD\n");
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("Client Error: ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("Client Error: ID_NO_FREE_INCOMING_CONNECTIONS\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			//printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_CONNECTION_LOST:
			printf("Client Error: ID_CONNECTION_LOST\n");
			break;
		case GAME_DATA:
			data = *(BoardMain*)p->data;
		}
		pClient->DeallocatePacket(p);
		p = pClient->Receive();

	}
};
void Client::Send(BoardMain* pC)
{

};

BoardMain Client::GetInput()
{
	return data;
};