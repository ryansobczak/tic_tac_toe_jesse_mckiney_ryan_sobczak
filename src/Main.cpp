#include <iostream>
#include "Server.h"
#include <vector>
#include <conio.h>

//#define ENTER_DOWN 
#define KEY_LEFT 75
#define KEY_RIGHT 77

void printToScreen(char charVec[]);
int input(char charVec[], int index, bool clientTurn);

int main()
{
	char charVec[9];
	int index = 0;
	bool clientTurn = true;
	bool gameRunning = true;

	for (size_t i = 0; i < 9; i++)
	{
		charVec[i] = ' ';
	}

	while (gameRunning)
	{
		printToScreen(charVec);

		index = input(charVec, index, clientTurn);
		printToScreen(charVec);
	}

	return 0;
}

int input(char charVec[], int index, bool clientTurn)
{
	int c = 0;
	c = _getch();
	if (c == KEY_RIGHT)
	{
		if (charVec[index + 1] == ' ' && index + 1 < 9)
		{
			index += 1;
			charVec[index] = '*';
		}
		//else if (charVec[index + 1] && index + 1 < 9)
		//{
		//	index += 1;
		//}
	}
	else if (c == KEY_LEFT)
	{
		if (charVec[index - 1] == ' ' && index - 1 >= 0)
		{
			index -= 1;
			charVec[index] = '*';
		}
		/*else if (charVec[index - 1] && index - 1 < 9)
		{
			index += 1;
		}*/
	}
	return index;
}

void printToScreen(char charVec[])
{
	system("cls");

	for (size_t i = 0; i < 9; i++)
	{
		std::cout << '|' << charVec[i];
		//if (i % 3 == 0)
			//std::cout << '|' << std::endl;
	}
}