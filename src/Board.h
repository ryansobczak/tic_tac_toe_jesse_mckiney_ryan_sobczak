#ifndef BOARD_H
#define BOARD_H


#pragma pack(push,1)
struct BoardMain
{
	bool URX;
	bool UMX;
	bool ULX;
	bool MRX;
	bool MMX;
	bool MLX;
	bool LRX;
	bool LMX;
	bool LLX;

	bool URO;
	bool UMO;
	bool ULO;
	bool MRO;
	bool MMO;
	bool MLO;
	bool LRO;
	bool LMO;
	bool LLO;
};
#pragma pack(pop)

enum UserPacket
{

	Gap = 134,
	GAME_DATA
};
#endif