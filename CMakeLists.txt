cmake_minimum_required(VERSION 2.8)
project(Root)
 
set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build)
 
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})

add_executable(tictactoe src/Main.cpp src/Server.h src/Server.cpp src/Client.h src/Client.cpp src/Board.h)
include_directories(RakNet/Source)
find_library(RAKNET_LIB
NAMES RakNetLibStatic
HINTS ${CMAKE_SOURCE_DIR}/RakNet)
target_link_libraries(tictactoe ${RAKNET_LIB} ws2_32)